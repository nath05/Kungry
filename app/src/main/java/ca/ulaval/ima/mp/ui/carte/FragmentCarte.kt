package ca.ulaval.ima.mp.ui.carte

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import ca.ulaval.ima.mp.ui.restaurant.RestaurantInfosActivity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentCarte : Fragment(), OnMapReadyCallback {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_carte, container, false)
        val cardInfo: CardView? = activity?.findViewById(R.id.card_info)
        cardInfo?.visibility = View.INVISIBLE
        val cardCreux: CardView? = activity?.findViewById(R.id.card_petitcreux)
        cardCreux?.visibility = View.VISIBLE
        val mapView: SupportMapFragment = root.findFragment()
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        return root
    }
    @SuppressLint("MissingPermission") //suppress as there is already a check in main activity
    override fun onMapReady(map: GoogleMap) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 12f))
        this.context?.let { getCloseByRestaurants(pos.latitude, pos.longitude, 50, map, it) }
        setUpLocationListener()
        map.isMyLocationEnabled=true
        var posi: LatLng= pos
        map.setOnMyLocationChangeListener(){
            if (kotlin.math.abs(pos.latitude - posi.latitude)>0.01){
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 12f))
                this.context?.let { it1 ->
                    getCloseByRestaurants(pos.latitude, pos.longitude, 50, map,
                        it1
                    )
                }
                posi= pos
            }
            if (kotlin.math.abs(pos.longitude - posi.longitude)>0.01){
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 12f))
                this.context?.let { it1 ->
                    getCloseByRestaurants(pos.latitude, pos.longitude, 50, map,
                        it1
                    )
                }
                posi= pos
            }


        }
        var hello: Marker = map.addMarker(MarkerOptions()
            .position(LatLng(0.0,0.0)))
        map.setOnMarkerClickListener(OnMarkerClickListener { marker ->
            this.context?.let {
                getRestaurants(marker.title.toInt(), marker.snippet.toDouble(),
                    it
                )
            }
            marker.setIcon(this.context?.let { bitmapDescriptorFromVector(it, R.drawable.ic_path_810) })
            if (hello.id != marker.id)
                hello.setIcon(this.context?.let { bitmapDescriptorFromVector(it, R.drawable.ic_pin_1) })
            hello = marker
            return@OnMarkerClickListener true
        })



    }
    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }
    @SuppressLint("MissingPermission") //suppress as there is already a check in main activity
    private fun setUpLocationListener() {
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.activity)
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest = LocationRequest().setInterval(2000).setFastestInterval(2000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    for (location in locationResult.locations) {
                        pos = LatLng(location.latitude, location.longitude)

                    }
                    // Few more things we can do here:
                    // For example: Update the location of user on server
                }
            },
            Looper.myLooper()
        )
    }
    private fun getCloseByRestaurants(
        latitude: Double,
        longitude: Double,
        radius: Int,
        map: GoogleMap,
        context: Context
    ){
        val kungryNetworkCenter=NetworkCenter.buildService(KungryAPI::class.java)
        kungryNetworkCenter.closeByRestaurants(latitude, longitude, radius).enqueue(object :
            Callback<KungryAPI.ContentResponse<CarteSerializer>> {
            override fun onResponse(
                call: Call<KungryAPI.ContentResponse<CarteSerializer>>,
                response: Response<KungryAPI.ContentResponse<CarteSerializer>>
            ) {
                response.body()?.content?.let {
                    val hue = 30f
                    for (i in it.results) {
                        map.addMarker(
                            MarkerOptions()
                                .icon(bitmapDescriptorFromVector(context, R.drawable.ic_pin_1))
                                //.icon(BitmapDescriptorFactory.defaultMarker(hue))
                                .position(LatLng(i.location.latitude, i.location.longitude))
                                .title("${i.id}")
                                .snippet("${i.distance}")
                        )
                    }

                }
            }

            override fun onFailure(
                call: Call<KungryAPI.ContentResponse<CarteSerializer>>,
                t: Throwable
            ) {
            }
        })
    }
    private fun getRestaurants(id: Int, dist: Double, context: Context){
        val kungryNetworkCenter=NetworkCenter.buildService(KungryAPI::class.java)
        val cardInfo: CardView? = activity?.findViewById(R.id.card_info)
        val cardName: TextView? = activity?.findViewById(R.id.cardName)
        val cardCuis: TextView? = activity?.findViewById(R.id.cardCuisine)
        val cardDist: TextView? = activity?.findViewById(R.id.cardDistance)
        val cardIm: ImageView? = activity?.findViewById(R.id.CardImage)
        val star1: ImageView? = activity?.findViewById(R.id.star1)
        val star2: ImageView? = activity?.findViewById(R.id.star2)
        val star3: ImageView? = activity?.findViewById(R.id.star3)
        val star4: ImageView? = activity?.findViewById(R.id.star4)
        val star5: ImageView? = activity?.findViewById(R.id.star5)
        val revCount: TextView? = activity?.findViewById(R.id.count)
        kungryNetworkCenter.getRestaurant(id).enqueue(object :
            Callback<KungryAPI.ContentResponse2> {
            override fun onResponse(
                call: Call<KungryAPI.ContentResponse2>,
                response: Response<KungryAPI.ContentResponse2>
            ) {
                response.body()?.content?.let { it ->
                    cardInfo?.visibility = View.VISIBLE
                    cardName?.text = it.name
                    for (i in it.cuisine)
                        cardCuis?.text = i.name
                    cardDist?.text = "$dist km"
                    Picasso.get().load(it.imageURL).into(cardIm)
                    if (it.reviewAverage.toInt() >= 1)
                        star1?.setColorFilter(Color.argb(255, 35, 210, 91))
                    if (it.reviewAverage.toInt() >= 2)
                        star2?.setColorFilter(Color.argb(255, 35, 210, 91))
                    if (it.reviewAverage.toInt() >= 3)
                        star3?.setColorFilter(Color.argb(255, 35, 210, 91))
                    if (it.reviewAverage.toInt() >= 4)
                        star4?.setColorFilter(Color.argb(255, 35, 210, 91))
                    if (it.reviewAverage.toInt() >= 5)
                        star5?.setColorFilter(Color.argb(255, 35, 210, 91))
                    revCount?.text = "(${it.reviewCount})"
                    cardInfo?.setOnClickListener { it2 ->
                        val intent = Intent(context, RestaurantInfosActivity::class.java)
                        val bundle = Bundle()
                        val restaurantId = it.id
                        bundle.putInt("id", restaurantId)
                        bundle.putDouble("lat", pos.latitude)
                        bundle.putDouble("long", pos.longitude)
                        intent.putExtra("Bundle", bundle)
                        startActivity(intent)
                    }
                }
            }

            override fun onFailure(
                call: Call<KungryAPI.ContentResponse2>,
                t: Throwable
            ) {
                Log.d("test", "big fail $t")
            }
        })
    }
    private fun bitmapDescriptorFromVector(
        context: Context,
        @DrawableRes vectorDrawableResourceId: Int
    ): BitmapDescriptor? {
        val background = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)

        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        background.draw(canvas)

        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
    companion object{
        var pos: LatLng = LatLng(0.0, 0.0)
    }
}