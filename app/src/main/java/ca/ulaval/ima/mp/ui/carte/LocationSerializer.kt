package ca.ulaval.ima.mp.ui.carte

import com.google.gson.annotations.SerializedName

data class LocationSerializer(
        @SerializedName("latitude") val latitude: Double,
        @SerializedName("longitude") val longitude: Double
)