package ca.ulaval.ima.mp.ui.evaluation

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.core.content.ContextCompat
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.MainActivity
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import ca.ulaval.ima.mp.ui.restaurant.RestaurantInfosActivity
import ca.ulaval.ima.mp.ui.restaurant.ReviewArrayAdapter
import ca.ulaval.ima.mp.ui.restaurant.ReviewSerializer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.roundToInt

class EvaluationListActivity : AppCompatActivity() {

    private val networkCenter = NetworkCenter.buildService(KungryAPI::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evaluation_list)

        val receivedBundle = intent.getBundleExtra("Bundle")
        val id = receivedBundle!!.getInt("id")
        val lat = receivedBundle.getDouble("lat")
        val long = receivedBundle.getDouble("long")

        val receivedReviews = ArrayList<ReviewSerializer>()
        val reviewList = findViewById<ListView>(R.id.all_evaluation_list)

        networkCenter.getRestaurantInfosById(id, lat, long).enqueue(object :
            Callback<KungryAPI.RestaurantResponse> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<KungryAPI.RestaurantResponse>,
                response: Response<KungryAPI.RestaurantResponse>
            ) {
                response.body()?.content?.let {
                    // on va chercher la réponse
                    val reponse = it

                    // on va chercher les éléments de la réponse
                    val reviews = reponse.reviews

                    for (evaluation in reviews) {
                        receivedReviews.add(evaluation)
                    }
                    val transformedReceivedReviews: Array<ReviewSerializer> =
                        receivedReviews.toTypedArray()
                    val adapter = ReviewArrayAdapter(
                        this@EvaluationListActivity,
                        R.layout.review_list_item,
                        transformedReceivedReviews
                    )
                    reviewList.adapter = adapter
                }
            }

            override fun onFailure(call: Call<KungryAPI.RestaurantResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "${t.message}", Toast.LENGTH_LONG).show()
            }
        })
    }
}