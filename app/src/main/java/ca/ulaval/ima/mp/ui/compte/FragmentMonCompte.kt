package ca.ulaval.ima.mp.ui.compte

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.cardview.widget.CardView
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import retrofit2.Call
import retrofit2.Response

class FragmentMonCompte : Fragment() {

    val KungryNetworkCenter = NetworkCenter.buildService(KungryAPI::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_mon_compte, container, false)

        val myAccountName = view.findViewById<TextView>(R.id.MyAccounteNameTV)
        val myAccountEmail = view.findViewById<TextView>(R.id.myAccountEmailTV)
        val myAccountNbrEvaluation = view.findViewById<TextView>(R.id.MyAccountNumberEvaluationTV)
        val disconnectButton = view.findViewById<Button>(R.id.disconnectMyaccountB)

        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(
            "token",
            Context.MODE_PRIVATE
        )
        val token: String? = sharedPreferences.getString("token", null)
        Log.d("demo", "token" + token.toString())



        KungryNetworkCenter.getDataUser("Bearer " + token, "API Test").enqueue(object :
            retrofit2.Callback<KungryAPI.ContentResponseMyAccount> {
            override fun onResponse(
                call: Call<KungryAPI.ContentResponseMyAccount>,
                response: Response<KungryAPI.ContentResponseMyAccount>
            ) {

                if (response.body()?.meta?.statusCode == 200) {
                    response.body()?.content.let {
                        myAccountName.text = it?.FirstName + it?.LastName
                        myAccountEmail.text = it?.email
                        myAccountNbrEvaluation.text = it?.reviewCount.toString()

                    }

                } else {
                    val fragment = FragmentCompte()
                    val fragmentManager = requireActivity().supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.nav_host_fragment, fragment)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }
            }

            override fun onFailure(call: Call<KungryAPI.ContentResponseMyAccount>, t: Throwable) {
                Log.d("demo", "erreur" + t.message.toString())
            }
        })

        disconnectButton.setOnClickListener() {
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.apply() {
                putString("token", "")
            }.apply()

            val fragment = FragmentCompte()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.nav_host_fragment, fragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }




        return view
    }
    override fun onResume() {
        super.onResume()
        val cardInfo: CardView? = activity?.findViewById(R.id.card_info)
        val cardCreux: CardView? = activity?.findViewById(R.id.card_petitcreux)
        cardInfo?.visibility = View.INVISIBLE
        cardCreux?.visibility = View.INVISIBLE
    }

}