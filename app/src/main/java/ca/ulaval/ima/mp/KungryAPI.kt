package ca.ulaval.ima.mp

import ca.ulaval.ima.mp.ui.carte.CarteSerializer
import ca.ulaval.ima.mp.ui.compte.myAccount
import ca.ulaval.ima.mp.ui.evaluation.evaluation
import ca.ulaval.ima.mp.ui.restaurant.RestaurantSerializer
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.*


interface KungryAPI {
    companion object {
        const val API_V1 = "api/v1/"
        const val BASE_URL: String = "https://kungry.ca/"
    }

    @GET(API_V1 + "restaurant/search/")
    fun closeByRestaurants(
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double,
        @Query("radius") radius: Int
    ): Call<ContentResponse<CarteSerializer>>

    @GET(API_V1 + "restaurant/{id}/")
    fun getRestaurant(
        @Path("id") id: Int
    ): Call<ContentResponse2>

    @GET(API_V1 + "account/me/")
    fun getDataUser(
        @Header("Authorization") token: String,
        @Header("User-Agent") userAgent: String
    ): Call<ContentResponseMyAccount>

    @POST(API_V1 + "account/")
    fun createAccount(@Body dataCreateAccount: HashMap<String, Any?>): Call<ContentResponseProfil>

    @POST(API_V1 + "account/login/")
    fun logInUser(
        @Body dataLogIn: HashMap<String, Any?>
    ):  Call<ContentResponseProfil>

    @POST(API_V1 + "review/")
    fun createEval(
        @Header("Authorization") token: String,
        @Body dataEval: HashMap<String, Any?>
    ):  Call<ContentResponseEvaluation>

    @GET(API_V1 + "restaurant/{id}")
    fun getRestaurantInfosById(@Path("id") id: Int,
                               @Query("latitude") latitude: Double,
                               @Query("longitude") longitude: Double): Call<RestaurantResponse>

    data class ContentResponse<T>(
        @SerializedName("content") val content: InsideContent<T>,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") val error: Error
    )

    data class ContentResponse2(
        @SerializedName("content") val content: RestaurantSerializer,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") val error: Error
    )

    data class ContentResponseProfil(
        @SerializedName("content") val content: token_output,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") val error: Error
    )

    data class ContentResponseMyAccount(
        @SerializedName("content") val content: myAccount,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") val error: Error
    )

    data class RestaurantResponse(
        @SerializedName("content") val content: RestaurantSerializer,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") val error: Error
    )

    data class ContentResponseEvaluation(
        @SerializedName("content") val content: evaluation,
        @SerializedName("meta") val meta: Meta,
        @SerializedName("error") val error: Error
    )

    data class Error(
        @SerializedName("display") val displayMessage: String
    )

    data class Meta(
        @SerializedName("status_code") val statusCode: Int
    )

    data class InsideContent<T>(
        @SerializedName("count") val count: Int?,
        @SerializedName("next") val next: Int?,
        @SerializedName("previous") val previous: Int?,
        @SerializedName("results") val results: List<T>
    )

    data class Evaluation<T>(
        @SerializedName("content") val content: ContentResponseEvaluation,
        @SerializedName("meta") val next: Meta,
        @SerializedName("error") val previous: Error,
    )


}