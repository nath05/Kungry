package ca.ulaval.ima.mp.ui.liste

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import ca.ulaval.ima.mp.ui.carte.CarteSerializer
import ca.ulaval.ima.mp.ui.carte.FragmentCarte
import ca.ulaval.ima.mp.ui.restaurant.RestaurantInfosActivity
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentListe : Fragment() {


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_liste, container, false)

        val kungryNetworkCenter= NetworkCenter.buildService(KungryAPI::class.java)

        val position = LatLng(FragmentCarte.pos.latitude, FragmentCarte.pos.longitude)
        val radius = 50

        val receivedRestaurants = ArrayList<CarteSerializer>()
        val restaurantList = root.findViewById<ListView>(R.id.restaurant_list)

        // on vient faire le call réseau pour aller chercher la liste des restaurants
        kungryNetworkCenter.closeByRestaurants(position.latitude, position.longitude, radius)
            .enqueue(object :
                Callback<KungryAPI.ContentResponse<CarteSerializer>> {
                override fun onResponse(
                    call: Call<KungryAPI.ContentResponse<CarteSerializer>>,
                    response: Response<KungryAPI.ContentResponse<CarteSerializer>>
                ) {
                    response.body()?.content?.let {
                        for (restaurant in it.results) {
                            receivedRestaurants.add(restaurant)
                        }
                        val transformedReveivedRestaurant: Array<CarteSerializer> =
                            receivedRestaurants.toTypedArray()
                        val adapter = RestaurantListArrayAdapter(
                            root.context as Activity,
                            R.layout.restaurant_list_item,
                            transformedReveivedRestaurant
                        )
                        restaurantList.adapter = adapter

                        restaurantList.onItemClickListener = AdapterView.OnItemClickListener { adapterView, activity_view, pos, id ->
                            val item = adapterView.getItemAtPosition(pos) as CarteSerializer
                            val intent = Intent(root.context, RestaurantInfosActivity::class.java)
                            val bundle = Bundle()
                            val restaurantId = item.id
                            bundle.putInt("id", restaurantId)
                            bundle.putDouble("lat", FragmentCarte.pos.latitude)
                            bundle.putDouble("long", FragmentCarte.pos.longitude)
                            intent.putExtra("Bundle", bundle)
                            startActivity(intent)
                        }
                    }
                }

                override fun onFailure(
                    call: Call<KungryAPI.ContentResponse<CarteSerializer>>,
                    t: Throwable
                ) {
                }
            })

        return root
    }
    override fun onResume() {
        super.onResume()
        val cardInfo: CardView? = activity?.findViewById(R.id.card_info)
        val cardCreux: CardView? = activity?.findViewById(R.id.card_petitcreux)
        cardInfo?.visibility = View.INVISIBLE
        cardCreux?.visibility = View.INVISIBLE
    }

}