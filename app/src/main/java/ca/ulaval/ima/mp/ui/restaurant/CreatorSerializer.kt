package ca.ulaval.ima.mp.ui.restaurant

import com.google.gson.annotations.SerializedName

data class CreatorSerializer(
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String
)
