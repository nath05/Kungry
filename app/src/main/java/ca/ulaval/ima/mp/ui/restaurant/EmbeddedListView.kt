package ca.ulaval.ima.mp.ui.restaurant

import android.content.Context
import android.util.AttributeSet
import android.widget.ListView

class EmbeddedListView(context: Context?, attrs: AttributeSet?) : ListView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(
            widthMeasureSpec, MeasureSpec.makeMeasureSpec(Int.MAX_VALUE shr 4, MeasureSpec.AT_MOST)
        )
    }
}