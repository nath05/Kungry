package ca.ulaval.ima.mp

import com.google.gson.annotations.SerializedName

data class token_output(
    @SerializedName("access_token") val token:String,
    @SerializedName("token_type") val tokenType:String,
    @SerializedName("refresh_token") val refreshToken:String,
    @SerializedName("scope") val scope:String,
    @SerializedName("expires_in") val expiresIn:Int
)