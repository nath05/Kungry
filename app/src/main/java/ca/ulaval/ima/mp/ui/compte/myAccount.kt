package ca.ulaval.ima.mp.ui.compte

import com.google.gson.annotations.SerializedName

data class myAccount(
    @SerializedName("total_review_count") val reviewCount:Int,
    @SerializedName("last_name") val LastName:String,
    @SerializedName("first_name") val FirstName:String,
    @SerializedName("email") val email:String,
    @SerializedName("created") val created:String,
    @SerializedName("updated") val updated:String,
    @SerializedName("user") val user:String
)
