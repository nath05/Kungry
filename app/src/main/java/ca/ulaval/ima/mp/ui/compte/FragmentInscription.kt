import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import ca.ulaval.ima.mp.ui.compte.FragmentCompte
import ca.ulaval.ima.mp.ui.compte.FragmentMonCompte
import ca.ulaval.ima.mp.ui.compte.myAccount
import retrofit2.Call
import retrofit2.Response


class FragmentInscription : Fragment() {

    val KungryNetworkCenter = NetworkCenter.buildService(KungryAPI::class.java)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_inscription, container, false)

        val firstName = view.findViewById<EditText>(R.id.signInFirstNameET)
        val lastName = view.findViewById<EditText>(R.id.signInLastNameET)
        val email = view.findViewById<EditText>(R.id.signInEmailET)
        val password = view.findViewById<EditText>(R.id.signInPasswordET)
        val signInButton = view.findViewById<Button>(R.id.signInButton)
        val clikableText1 = view.findViewById<TextView>(R.id.clickableTexte1TV)
        val clikableText2 = view.findViewById<TextView>(R.id.clickableTexte2TV)






        signInButton.setOnClickListener() {


            val dataCreateAccount = hashMapOf<String, Any?>(
                "client_id" to "STO4WED2NTDDxjLs8ODios5M15HwsrRlydsMa1t0",
                "client_secret" to "YOVWGpjSnHd5AYDxGBR2CIB09ZYM1OPJGnH3ijkKwrUMVvwLprUmLf6fxku06ClUKTAEl5AeZN36V9QYBYvTtrLMrtUtXVuXOGWleQGYyApC2a469l36TdlXFqAG1tpK",
                "first_name" to firstName.text.toString(),
                "last_name" to lastName.text.toString(),
                "email" to email.text.toString(),
                "password" to password.text.toString()
            )
            Log.d("demo", "$dataCreateAccount")


            KungryNetworkCenter.createAccount(
                dataCreateAccount
            ).enqueue(object :
                retrofit2.Callback<KungryAPI.ContentResponseProfil> {
                override fun onResponse(
                    call: Call<KungryAPI.ContentResponseProfil>,
                    response: Response<KungryAPI.ContentResponseProfil>
                ) {
                    Log.d("demo", response.body()?.meta?.statusCode.toString())
                    if (response.body()?.meta?.statusCode == 200) {

                        response.body()?.content.let {
//                        Enregistement des parametres du token de retour apres la creation d un compte
//                        Info: access token, token Type, scope, expire in
                            Log.d("demo", it?.token.toString())
                            val sharedPreferences: SharedPreferences =
                                requireActivity().getSharedPreferences(
                                    "token",
                                    Context.MODE_PRIVATE
                                )
                            val editor: SharedPreferences.Editor = sharedPreferences.edit()
                            editor.apply() {
                                putString("token", it?.token.toString())
                            }.apply()
                            Log.d("demo", it?.token.toString())


                            val fragment1 = FragmentMonCompte()
                            val fragmentManager = requireActivity().supportFragmentManager
                            val fragmentTransaction = fragmentManager.beginTransaction()
                            fragmentTransaction.replace(R.id.nav_host_fragment, fragment1)
                            fragmentTransaction.addToBackStack(null)
                            fragmentTransaction.commit()


                        }

                    }
                }

                override fun onFailure(
                    call: Call<KungryAPI.ContentResponseProfil>,
                    t: Throwable
                ) {
                    Log.d("demo", "erreur" + t.message.toString())
                }
            })

        }


        clikableText1.setOnClickListener() {
            Log.d("demo", "ouaip")
            changeFragment()
        }
        clikableText2.setOnClickListener() {
            Log.d("demo", "ouaip 2")
            changeFragment()
        }

        return view

    }

    fun changeFragment() {
        //Demande de changement de fragment pour le fragment de connexion
        val fragment = FragmentCompte()
        val fragmentManager = requireActivity().supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }


}