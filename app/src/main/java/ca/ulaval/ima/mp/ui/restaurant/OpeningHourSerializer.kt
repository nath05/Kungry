package ca.ulaval.ima.mp.ui.restaurant

import com.google.gson.annotations.SerializedName

data class OpeningHourSerializer(
    @SerializedName("id") val id: Int,
    @SerializedName("opening_hour") val openingHour: String,
    @SerializedName("closing_hour") val closingHour: String,
    @SerializedName("day") val day: String
)