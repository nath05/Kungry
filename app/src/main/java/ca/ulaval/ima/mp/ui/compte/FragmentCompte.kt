package ca.ulaval.ima.mp.ui.compte

import FragmentInscription
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import retrofit2.Call
import retrofit2.Response

class FragmentCompte : Fragment() {

    val KungryNetworkCenter = NetworkCenter.buildService(KungryAPI::class.java)


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_compte, container, false)

        val email = view.findViewById<EditText>(R.id.logInEmailET)
        val password = view.findViewById<EditText>(R.id.logInPasswordET)
        val logInButton = view.findViewById<Button>(R.id.logInButton)
        val clikableText1 = view.findViewById<TextView>(R.id.clickableTexteCompte1TV)
        val clikableText2 = view.findViewById<TextView>(R.id.clickableTexteCompte2TV)

        logInButton.setOnClickListener() {

            val dataLogIn = hashMapOf<String, Any?>(
                "client_id" to "STO4WED2NTDDxjLs8ODios5M15HwsrRlydsMa1t0",
                "client_secret" to "YOVWGpjSnHd5AYDxGBR2CIB09ZYM1OPJGnH3ijkKwrUMVvwLprUmLf6fxku06ClUKTAEl5AeZN36V9QYBYvTtrLMrtUtXVuXOGWleQGYyApC2a469l36TdlXFqAG1tpK",
                "email" to email.text.toString(),
                "password" to password.text.toString()
            )

            val sharedPreferences: SharedPreferences =
                requireActivity().getSharedPreferences(
                    "token",
                    Context.MODE_PRIVATE
                )
            val token: String? = sharedPreferences.getString("token", null)



            KungryNetworkCenter.logInUser(dataLogIn).enqueue(object :
                retrofit2.Callback<KungryAPI.ContentResponseProfil> {
                override fun onResponse(
                    call: Call<KungryAPI.ContentResponseProfil>,
                    response: Response<KungryAPI.ContentResponseProfil>
                ) {
                    Log.d("demo", response.body()?.meta?.statusCode.toString())
                    if (response.body()?.meta?.statusCode?.toInt() == 200) {
                        val editor: SharedPreferences.Editor = sharedPreferences.edit()
                        editor.apply() {
                            putString("token", response.body()?.content?.token.toString())
                        }.apply()
                    }
                    val fragment2 = FragmentMonCompte()
                    val fragmentManager = requireActivity().supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.nav_host_fragment, fragment2)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()

                }

                override fun onFailure(call: Call<KungryAPI.ContentResponseProfil>, t: Throwable) {
                    Log.d("demo", t.message.toString())
                }
            })


        }

        clikableText1.setOnClickListener() {
            changeFragment()

        }
        clikableText2.setOnClickListener() {
            changeFragment()
        }

        return view
    }

    private fun changeFragment() {
        val fragment = FragmentInscription()
        val fragmentManager = requireActivity().supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
    override fun onResume() {
        super.onResume()
        val cardInfo: CardView? = activity?.findViewById(R.id.card_info)
        val cardCreux: CardView? = activity?.findViewById(R.id.card_petitcreux)
        cardInfo?.visibility = View.INVISIBLE
        cardCreux?.visibility = View.INVISIBLE
    }
}