package ca.ulaval.ima.mp.ui.restaurant

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.MainActivity
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.R
import ca.ulaval.ima.mp.ui.evaluation.EvaluationActivity
import ca.ulaval.ima.mp.ui.evaluation.EvaluationListActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.roundToInt


class RestaurantInfosActivity : AppCompatActivity(), OnMapReadyCallback {

    private val networkCenter = NetworkCenter.buildService(KungryAPI::class.java)

    private lateinit var map: GoogleMap

    companion object{
        var position: LatLng = LatLng(0.0, 0.0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant)

        val receivedBundle = intent.getBundleExtra("Bundle")
        val id = receivedBundle!!.getInt("id")
        val lat = receivedBundle.getDouble("lat")
        val long = receivedBundle.getDouble("long")
        val sharedPreferences: SharedPreferences = getSharedPreferences("token", Context.MODE_PRIVATE)
        val token: String? = sharedPreferences.getString("token", null)
        var isConnected = false
        if(!token.isNullOrEmpty()){
            isConnected = true
        }

        val phoneButton = findViewById<Button>(R.id.call_button)
        val websiteButton = findViewById<Button>(R.id.website_button)

        val restaurantImage = findViewById<ImageView>(R.id.restaurant_image)

        val mondayHourText = findViewById<TextView>(R.id.opening_hours_monday)
        val tuesdayHourText = findViewById<TextView>(R.id.opening_hours_tuesday)
        val wednesdayHourText = findViewById<TextView>(R.id.opening_hours_wednesday)
        val thursdayHourText = findViewById<TextView>(R.id.opening_hours_thursday)
        val fridayHourText = findViewById<TextView>(R.id.opening_hours_friday)
        val saturdayHourText = findViewById<TextView>(R.id.opening_hours_saturday)
        val sundayHourText = findViewById<TextView>(R.id.opening_hours_sunday)

        val mapView: SupportMapFragment = supportFragmentManager.findFragmentById(R.id.small_map) as SupportMapFragment
        mapView.getMapAsync(this)

        val receivedReviews = ArrayList<ReviewSerializer>()
        val reviewList = findViewById<ListView>(R.id.evaluation_list)

        // on va chercher les infos du restaurant avec l'ID reçu
        networkCenter.getRestaurantInfosById(id, lat, long).enqueue(object :
            Callback<KungryAPI.RestaurantResponse> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<KungryAPI.RestaurantResponse>,
                response: Response<KungryAPI.RestaurantResponse>
            ) {
                response.body()?.content?.let {
                    // on va chercher la réponse
                    val reponse = it

                    // on va chercher les éléments de la réponse
                    val restaurantId = reponse.id
                    val cuisine = reponse.cuisine[0].name
                    val distance = reponse.distance.toString()
                    val reviewCount = reponse.reviewCount.toString()
                    val openingHours = reponse.openingHours
                    val reviewAverage = reponse.reviewAverage.toDouble()
                    val location = reponse.location
                    val reviews = reponse.reviews
                    val restaurantName = reponse.name
                    val websiteURL = reponse.website
                    val phoneNumber = reponse.phoneNumber
                    val imageURL = reponse.imageURL
                    var type = reponse.type

                    // on va mettre à jour les valeurs dans le layout d'info de restos
                    Picasso.get().load(imageURL).fit().centerCrop().into(restaurantImage)

                    val restaurantNameText = findViewById<TextView>(R.id.restaurant_name)
                    restaurantNameText.text = restaurantName

                    val formattedPhoneNumber = "(${phoneNumber[0]}${phoneNumber[1]}${phoneNumber[2]}) ${phoneNumber[3]}${phoneNumber[4]}${phoneNumber[5]}-${phoneNumber[6]}${phoneNumber[7]}${phoneNumber[8]}${phoneNumber[9]}"
                    phoneButton.text = formattedPhoneNumber

                    val restaurantTypeText = findViewById<TextView>(R.id.restaurant_type)
                    when (type) {
                        "RESTO" -> type = "Restaurant"
                        "BAR" -> type = "Bar"
                        "SNACK" -> type = "Snack"
                    }
                    restaurantTypeText.text = "$type - $cuisine"

                    val distanceText = findViewById<TextView>(R.id.distance)
                    distanceText.text = "$distance km"

                    val reviewCountText = findViewById<TextView>(R.id.review_count)
                    reviewCountText.text = reviewCount

                    when (reviewAverage.roundToInt()) {
                        1 -> {
                            val star1 = findViewById<ImageView>(R.id.star1)
                            star1.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                        }
                        2 -> {
                            val star1 = findViewById<ImageView>(R.id.star1)
                            val star2 = findViewById<ImageView>(R.id.star2)
                            star1.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star2.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                        }
                        3 -> {
                            val star1 = findViewById<ImageView>(R.id.star1)
                            val star2 = findViewById<ImageView>(R.id.star2)
                            val star3 = findViewById<ImageView>(R.id.star3)
                            star1.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star2.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star3.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                        }
                        4 -> {
                            val star1 = findViewById<ImageView>(R.id.star1)
                            val star2 = findViewById<ImageView>(R.id.star2)
                            val star3 = findViewById<ImageView>(R.id.star3)
                            val star4 = findViewById<ImageView>(R.id.star4)
                            star1.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star2.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star3.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star4.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                        }
                        5 -> {
                            val star1 = findViewById<ImageView>(R.id.star1)
                            val star2 = findViewById<ImageView>(R.id.star2)
                            val star3 = findViewById<ImageView>(R.id.star3)
                            val star4 = findViewById<ImageView>(R.id.star4)
                            val star5 = findViewById<ImageView>(R.id.star5)
                            star1.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star2.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star3.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star4.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                            star5.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.star_green
                                ), android.graphics.PorterDuff.Mode.MULTIPLY
                            )
                        }
                    }

                    phoneButton.setOnClickListener {
                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.fromParts("tel", phoneNumber, null)
                        startActivity(intent)
                    }

                    websiteButton.setOnClickListener {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(websiteURL))
                        startActivity(intent)
                    }

                    if (openingHours.isNullOrEmpty()) {
                        mondayHourText.text = "Fermé"
                        tuesdayHourText.text = "Fermé"
                        wednesdayHourText.text = "Fermé"
                        thursdayHourText.text = "Fermé"
                        fridayHourText.text = "Fermé"
                        saturdayHourText.text = "Fermé"
                        sundayHourText.text = "Femré"
                    } else {
                        openingHours.forEach { it2 ->
                            when (it2.day) {
                                "SUN" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        sundayHourText.text = "Fermé"
                                    } else {
                                        sundayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                                "MON" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        mondayHourText.text = "Fermé"
                                    } else {
                                        mondayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                                "TUE" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        tuesdayHourText.text = "Fermé"
                                    } else {
                                        tuesdayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                                "WED" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        wednesdayHourText.text = "Fermé"
                                    } else {
                                        wednesdayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                                "THU" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        thursdayHourText.text = "Fermé"
                                    } else {
                                        thursdayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                                "FRI" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        fridayHourText.text = "Fermé"
                                    } else {
                                        fridayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                                "SAT" -> {
                                    if (it2.openingHour.isNullOrEmpty() && it2.closingHour.isNullOrEmpty()) {
                                        saturdayHourText.text = "Fermé"
                                    } else {
                                        saturdayHourText.text =
                                            "${it2.openingHour.substring(0, 5)} à ${
                                                it2.closingHour.substring(
                                                    0,
                                                    5
                                                )
                                            }"
                                    }
                                }
                            }
                        }
                    }

                    position = LatLng(location.latitude, location.longitude)
                    map.addMarker(MarkerOptions().position(position).icon(customGoogleMapPin(applicationContext, R.drawable.ic_icone_pin_big)).title(restaurantName))
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16f))

                    val evaluationText = findViewById<TextView>(R.id.evaluation)
                    evaluationText.text = "($reviewCount)"

                    for (evaluation in reviews) {
                        receivedReviews.add(evaluation)
                    }
                    val transformedReceivedReviews: Array<ReviewSerializer> =
                        receivedReviews.toTypedArray()
                    val adapter = ReviewArrayAdapter(
                        this@RestaurantInfosActivity,
                        R.layout.review_list_item,
                        transformedReceivedReviews
                    )
                    reviewList.adapter = adapter

                    val moreEvaluation = findViewById<TextView>(R.id.more_evaluation)
                    moreEvaluation.setOnClickListener {
                        val intent = Intent(applicationContext, EvaluationListActivity::class.java)
                        val bundle = Bundle()
                        bundle.putInt("id", restaurantId)
                        bundle.putDouble("lat", lat)
                        bundle.putDouble("long", long)
                        intent.putExtra("Bundle", bundle)
                        startActivity(intent)
                    }

                    val backButton = findViewById<ImageButton>(R.id.back_button)
                    backButton.setOnClickListener{
                        finish()
                    }

                    val connectionText = findViewById<TextView>(R.id.connection_text)
                    val connectionButton = findViewById<Button>(R.id.connection_button)
                    if(!isConnected){
                        connectionButton.setOnClickListener {
                            // on vient starté le fragment pour gérer la connexion
                            val intent = Intent(applicationContext, MainActivity::class.java)
                            val bundle = Bundle()
                            bundle.putString("Connection", "connection")
                            intent.putExtra("Bundle", bundle)
                            startActivity(intent)
                        }
                    }
                    else{
                        // on transforme le layout du texte et du bouton et on call l'activité de laisser une évaluation
                        connectionText.text = ""
                        connectionButton.text = getString(R.string.is_connected)
                        connectionButton.setBackgroundColor(resources.getColor(R.color.black))
                        connectionButton.setOnClickListener {
                            val intent = Intent(application, EvaluationActivity::class.java)
                            val bundle = Bundle()
                            bundle.putInt("id", restaurantId)
                            intent.putExtra("Bundle", bundle)
                            startActivity(intent)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<KungryAPI.RestaurantResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "${t.message}", Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
    }

    private fun customGoogleMapPin(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(0,0, vectorDrawable.intrinsicWidth , vectorDrawable.intrinsicHeight)
        val bitmap: Bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}