package ca.ulaval.ima.mp.ui.evaluation

import com.google.gson.annotations.SerializedName

data class creator(
    @SerializedName("first_name")val firstName: String,
    @SerializedName("last_name")val lastName: String
)
