package ca.ulaval.ima.mp.ui.evaluation

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import ca.ulaval.ima.mp.R
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import ca.ulaval.ima.mp.KungryAPI
import ca.ulaval.ima.mp.NetworkCenter
import ca.ulaval.ima.mp.ui.compte.FragmentMonCompte

class EvaluationActivity : AppCompatActivity() {

    val KungryNetworkCenter = NetworkCenter.buildService(KungryAPI::class.java)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evaluation)
        setSupportActionBar(findViewById(R.id.toolbar))



        //val view = inflater.inflate(R.layout.evaluation, container, false)


        val comment = findViewById<EditText>(R.id.eval_comment)
        val submitButton = findViewById<Button>(R.id.submit_button)

        submitButton.setOnClickListener() {
            val eval_id = intent.getStringExtra("restaurantId")
            val dataEvaluation = hashMapOf<String, Any?>(
                "restaurant_id" to eval_id,
                "stars" to "4",
                "comment" to comment.text.toString()
            )

            val sharedPreferences: SharedPreferences =
                getSharedPreferences(
                    "token",
                    Context.MODE_PRIVATE
                )
            val token: String? = sharedPreferences.getString("token", null)



            KungryNetworkCenter.createEval("Bearer " + token, dataEvaluation).enqueue(object :
                retrofit2.Callback<KungryAPI.ContentResponseEvaluation> {
                override fun onResponse(
                    call: Call<KungryAPI.ContentResponseEvaluation>,
                    response: Response<KungryAPI.ContentResponseEvaluation>
                ) {


                }

                override fun onFailure(call: Call<KungryAPI.ContentResponseEvaluation>, t: Throwable) {
                    Log.d("demo", t.message.toString())
                }
            })


        }


}
}