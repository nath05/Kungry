package ca.ulaval.ima.mp

import android.annotation.SuppressLint
import android.app.Notification
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.view.Gravity
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import ca.ulaval.ima.mp.ui.liste.FragmentListe
import ca.ulaval.ima.mp.ui.carte.FragmentCarte
import ca.ulaval.ima.mp.ui.carte.PermissionUtils
import ca.ulaval.ima.mp.ui.compte.FragmentCompte
import ca.ulaval.ima.mp.ui.compte.FragmentMonCompte
import com.google.android.gms.location.*

class MainActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)


        val navController = findNavController(R.id.nav_host_fragment)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setIcon(R.drawable.ic_logo_kungry)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_carte, R.id.navigation_liste, R.id.navigation_compte
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setOnNavigationItemSelectedListener(this)

        try {
            // si on reçoit un bundle, ça veut dire qu'on doit aller sur le fragment compte, autrement on va sur le fragment carte
            val bundle = intent.getBundleExtra("Bundle")
            val connection = bundle!!.getString("Connection")
            navView.selectedItemId = R.id.navigation_compte
        } catch (e: Exception) {
            //set an initial fragment
            navView.selectedItemId = R.id.navigation_carte
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val sharedPreferences: SharedPreferences = getSharedPreferences(
            "token",
            Context.MODE_PRIVATE
        )
        val token: String? = sharedPreferences.getString("token", null)

        lateinit var fragment: Fragment
        when (item.itemId) {
            R.id.navigation_liste -> {
                fragment = FragmentListe()
            }
            R.id.navigation_carte -> {
                fragment = FragmentCarte()
            }
            R.id.navigation_compte -> {
                if (token == "") {
                    fragment = FragmentCompte()
                } else {
                    fragment = FragmentMonCompte()
                }

            }
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment)
        fragmentTransaction.commit()

        return true
    }

    override fun onStart() {
        super.onStart()
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {
                        setUpLocationListener()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                val LOCATION_PERMISSION_REQUEST_CODE = 0
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun setUpLocationListener() {
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest = LocationRequest().setInterval(2000).setFastestInterval(2000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    for (location in locationResult.locations) {
                        val lat = location.latitude.toString()
                        val long = location.longitude.toString()
                        //Log.d("test","$lat and $long")
                    }
                    // Few more things we can do here:
                    // For example: Update the location of user on server
                }
            },
            Looper.myLooper()
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val LOCATION_PERMISSION_REQUEST_CODE = 0
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

}
