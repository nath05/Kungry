package ca.ulaval.ima.mp.ui.restaurant

import ca.ulaval.ima.mp.ui.carte.CuisineSerializer
import ca.ulaval.ima.mp.ui.carte.LocationSerializer
import com.google.gson.annotations.SerializedName

class RestaurantSerializer(
    @SerializedName("id") val id: Int,
    @SerializedName("cuisine") val cuisine: Array<CuisineSerializer>,
    @SerializedName("distance") val distance: Double,
    @SerializedName("review_count") val reviewCount: Int,
    @SerializedName("opening_hours") val openingHours: Array<OpeningHourSerializer>,
    @SerializedName("review_average") val reviewAverage: Number,
    @SerializedName("location") val location: LocationSerializer,
    @SerializedName("reviews") val reviews: Array<ReviewSerializer>,
    @SerializedName("name") val name: String,
    @SerializedName("website") val website: String,
    @SerializedName("phone_number") val phoneNumber: String,
    @SerializedName("image") val imageURL: String,
    @SerializedName("type") val type: String
)