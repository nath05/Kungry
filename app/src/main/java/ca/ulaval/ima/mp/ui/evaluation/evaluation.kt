package ca.ulaval.ima.mp.ui.evaluation

import com.google.gson.annotations.SerializedName

data class evaluation(
    @SerializedName("id") val id: Int,
    @SerializedName("creator") val creator: creator,
    @SerializedName("stars") val stars: Int,
    @SerializedName("image") val image: String,
    @SerializedName("comment") val comment: String,
    @SerializedName("date") val date: String
)
