package ca.ulaval.ima.mp.ui.restaurant

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import ca.ulaval.ima.mp.R
import com.squareup.picasso.Picasso

class ReviewArrayAdapter(val context: Activity, private val cellRessourceId: Int, private val reviews: Array<ReviewSerializer>): ArrayAdapter<ReviewSerializer>(context, cellRessourceId, reviews) {
    override fun getCount(): Int {
        return reviews.size
    }

    override fun getItem(position: Int): ReviewSerializer {
        return reviews[position]
    }

    inner class ViewHolder {
        lateinit var reviewImage: ImageView
        lateinit var reviewStar1: ImageView
        lateinit var reviewStar2: ImageView
        lateinit var reviewStar3: ImageView
        lateinit var reviewStar4: ImageView
        lateinit var reviewStar5: ImageView
        lateinit var date: TextView
        lateinit var reviewerName: TextView
        lateinit var reviewComment: TextView
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var line = convertView
        val holder: ViewHolder
        if(line == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            line = inflater.inflate(cellRessourceId, parent, false)
            holder = ViewHolder().also {
                it.reviewImage = line.findViewById(R.id.review_image)
                it.reviewStar1 = line.findViewById(R.id.review_star1)
                it.reviewStar2 = line.findViewById(R.id.review_star2)
                it.reviewStar3 = line.findViewById(R.id.review_star3)
                it.reviewStar4 = line.findViewById(R.id.review_star4)
                it.reviewStar5 = line.findViewById(R.id.review_star5)
                it.date = line.findViewById(R.id.review_date)
                it.reviewerName = line.findViewById(R.id.reviewer_name)
                it.reviewComment = line.findViewById(R.id.review_comment)
            }
            line.tag = holder
        }
        else holder = line.tag as ViewHolder

        val review = getItem(position)

        Picasso.get().load(review.imageURL).resize(360, 360).into(holder.reviewImage)
        holder.date.text = review.date
        holder.reviewerName.text = "${review.creator.firstName} ${review.creator.lastName}"
        holder.reviewComment.text = review.comment
        val reviewScore = review.stars
        when(reviewScore){
            1 -> {
                holder.reviewStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            2 -> {
                holder.reviewStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            3 -> {
                holder.reviewStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar3.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            4 -> {
                holder.reviewStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar3.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar4.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            5 -> {
                holder.reviewStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar3.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar4.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.reviewStar5.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
        }
        return line!!
    }
}