package ca.ulaval.ima.mp.ui.carte

import com.google.gson.annotations.SerializedName

data class CarteSerializer(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("cuisine") val cuisine: List<CuisineSerializer>,
    @SerializedName("type") val type: String,
    @SerializedName("review_count") val reviewCount: Int,
    @SerializedName("review_average") val reviewAvg: Double,
    @SerializedName("image") val image: String,
    @SerializedName("distance") val distance: Double,
    @SerializedName("location") val location: LocationSerializer
)