package ca.ulaval.ima.mp.ui.restaurant

import com.google.gson.annotations.SerializedName

data class ReviewSerializer(
    @SerializedName("id") val id: Int,
    @SerializedName("creator") val creator: CreatorSerializer,
    @SerializedName("stars") val stars: Int,
    @SerializedName("image") val imageURL: String,
    @SerializedName("comment") val comment: String,
    @SerializedName("date") val date: String
)
