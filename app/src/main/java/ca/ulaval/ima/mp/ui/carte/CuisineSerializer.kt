package ca.ulaval.ima.mp.ui.carte

import com.google.gson.annotations.SerializedName

data class CuisineSerializer(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String
)