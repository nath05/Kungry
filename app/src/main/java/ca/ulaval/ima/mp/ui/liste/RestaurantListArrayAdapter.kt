package ca.ulaval.ima.mp.ui.liste

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import ca.ulaval.ima.mp.R
import ca.ulaval.ima.mp.ui.carte.CarteSerializer
import com.squareup.picasso.Picasso
import kotlin.math.roundToInt

class RestaurantListArrayAdapter(val context: Activity, private val cellRessourceId: Int, private val restaurants: Array<CarteSerializer>): ArrayAdapter<CarteSerializer>(context, cellRessourceId, restaurants) {
    override fun getCount(): Int {
        return restaurants.size
    }

    override fun getItem(position: Int): CarteSerializer {
        return restaurants[position]
    }

    inner class ViewHolder {
        lateinit var restaurantImage: ImageView
        lateinit var restaurantStar1: ImageView
        lateinit var restaurantStar2: ImageView
        lateinit var restaurantStar3: ImageView
        lateinit var restaurantStar4: ImageView
        lateinit var restaurantStar5: ImageView
        lateinit var restaurantReviewCount: TextView
        lateinit var restaurantName: TextView
        lateinit var restaurantType: TextView
        lateinit var restaurantDistance: TextView
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var line = convertView
        val holder: ViewHolder
        if(line == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            line = inflater.inflate(cellRessourceId, parent, false)
            holder = ViewHolder().also {
                it.restaurantImage = line.findViewById(R.id.restaurant_list_image)
                it.restaurantStar1 = line.findViewById(R.id.restaurant_list_star1)
                it.restaurantStar2 = line.findViewById(R.id.restaurant_list_star2)
                it.restaurantStar3 = line.findViewById(R.id.restaurant_list_star3)
                it.restaurantStar4 = line.findViewById(R.id.restaurant_list_star4)
                it.restaurantStar5 = line.findViewById(R.id.restaurant_list_star5)
                it.restaurantReviewCount = line.findViewById(R.id.restaurant_list_reviewCount)
                it.restaurantName = line.findViewById(R.id.restaurant_list_name)
                it.restaurantType = line.findViewById(R.id.restaurant_list_type)
                it.restaurantDistance = line.findViewById(R.id.restaurant_list_distance)
            }
            line.tag = holder
        }
        else holder = line.tag as ViewHolder

        val restaurant = getItem(position)

        Picasso.get().load(restaurant.image).resize(360, 360).into(holder.restaurantImage)
        holder.restaurantName.text = restaurant.name
        holder.restaurantReviewCount.text = "(${restaurant.reviewCount})"
        when(restaurant.type){
            "RESTO" -> holder.restaurantType.text = "Restaurant - ${restaurant.cuisine[0].name}"
            "BAR" -> holder.restaurantType.text = "Bar -${restaurant.cuisine[0].name}"
            "SNACK" -> holder.restaurantType.text = "Snack -${restaurant.cuisine[0].name}"
        }

        holder.restaurantDistance.text = "${restaurant.distance} km"
        when(restaurant.reviewAvg.roundToInt()){
            1 -> {
                holder.restaurantStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            2 -> {
                holder.restaurantStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            3 -> {
                holder.restaurantStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar3.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            4 -> {
                holder.restaurantStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar3.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar4.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
            5 -> {
                holder.restaurantStar1.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar2.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar3.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar4.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
                holder.restaurantStar5.setColorFilter(ContextCompat.getColor(context, R.color.star_green), android.graphics.PorterDuff.Mode.MULTIPLY)
            }
        }
        return line!!
    }
}